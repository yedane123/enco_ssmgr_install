sudo ln -sf ../usr/share/zoneinfo/Europe/Paris /etc/localtime
sudo yum -y install vim wget
sudo yum -y groupinstall "Development Tools"


cat > .bashrc << EOF
alias rm='rm -i'
alias cp='cp -i'
alias mv='mv -i'
alias vi='/usr/bin/vim'
alias ll='ls -lrta'
alias jailList="fail2ban-client status | sed -n 's/,//g;s/.*Jail list://p' | xargs -n1 fail2ban-client status"

# Source global definitions
if [ -f /etc/bashrc ]; then
    . /etc/bashrc
fi
EOF

cat > .vimrc << EOF
syntax on
set tabstop=4
EOF

wget --no-check-certificate https://gitlab.com/yedane123/enco_ssmgr_install/raw/master/ssmgr-install.sh
sudo chmod +x ssmgr-install.sh

wget -N --no-check-certificate https://raw.githubusercontent.com/FunctionClub/ZBench/master/ZBench-CN.sh
